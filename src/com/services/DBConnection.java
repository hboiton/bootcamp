/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author hboiton
 */
//jdbc:mariadb://localhost:3306/
final class DBConnection {
    private static final String DB_URL = "jdbc:mariadb://localhost:3306/books";
    private static final String DB_USER = "root";
    private static final String DB_PWD = "PcLCIn0Ff4";
    
    public static Connection getConnection(){
        try {
            return DriverManager.getConnection(DB_URL,DB_USER,DB_PWD);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
            
    
}
