/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services;
import com.services.DBConnection;
import com.data.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author hboiton
 */

final class MyCustomExeption  extends RuntimeException{

    public MyCustomExeption(){
        super();
    }
    
    public MyCustomExeption (String msg){
        super(msg);
    }
    
    public MyCustomExeption (String msg, Throwable cause){
        super(msg, cause);
    }
    
    public MyCustomExeption (Throwable cause){
        super(cause);
    }
}
final class Dao {
    
    
    public Dao ( ){ }
    
    public int setData(Book data) {
        String sql = "INSERT INTO book (title, author, gender) VALUES (?,?,?)";
        int resutl=0;
        try (Connection connection = DBConnection.getConnection();PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, data.getTitle());
                stmt.setString(2, data.getAuthor());
                stmt.setString(3, data.getGender());
                stmt.executeUpdate();
                resutl = 1;
            } catch (SQLException e) {
                throw new MyCustomExeption("ERROR", e.getCause());
           }finally{
                return resutl;
           }
    }
    
    public int modifyData(Book data) {
        String sql = "UPDATE book SET author = ? ,gender = ? WHERE title = ?";
        int resutl=0;
        try (Connection connection = DBConnection.getConnection();PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, data.getAuthor());
                stmt.setString(2, data.getGender());
                stmt.setString(3, data.getTitle());
                stmt.executeUpdate();
            resutl = 1;
            } catch (SQLException e) {
                throw new MyCustomExeption("ERROR", e.getCause());
           }finally{
                return resutl;
           }
    }
    
     public int deleteData(String title) {
        String sql = "DELETE FROM book WHERE title = ?";
        int resutl=0;
        try (Connection connection = DBConnection.getConnection();PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, title);
                stmt.executeUpdate();
                resutl = 1;
         } catch (SQLException e) {
                throw new MyCustomExeption("ERROR", e.getCause());
        }finally{
                return resutl;
        }
    }
     
    public Book serch (String title ){
        Book book = null;
        String sql = "SELECT * FROM book where title = "+ title;
        try (Connection connection = DBConnection.getConnection();PreparedStatement stmt = connection.prepareStatement(sql)) {            
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()){
               book = new Book();
               book.setId(resultSet.getInt("id"));
               book.setTile(resultSet.getString("title"));
               book.setAuthor(resultSet.getString("author"));
               book.setGender(resultSet.getString("gender"));
            }
            resultSet.close();
        } catch (SQLException e) {
             throw new MyCustomExeption("ERROR", e.getCause());
        }finally{
            return book;
        }
        
    }
    
    public ArrayList<Book> serchAll (){
        ArrayList<Book> books = null;
        Book book = null;
        String sql = "SELECT * FROM book";
        try (Connection connection = DBConnection.getConnection();PreparedStatement stmt = connection.prepareStatement(sql)) {
            ResultSet resultSet = stmt.executeQuery();
            books = new ArrayList<Book>();
            while (resultSet.next()){
               book = new Book();
               books.add(book);
            }
            resultSet.close();
        } catch (SQLException e) {
             throw new MyCustomExeption("ERROR", e.getCause());
        }finally{
            return books;
        }
        
    } 
    
    
}
