/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.services;
import com.services.Dao;
import com.data.Book;
import java.util.ArrayList;

/**
 *
 * @author hboiton
 */
public class Service {
    private Dao dao;
    public Service(){}
    
    public int save(Book book){
        int result=0;
        dao= new Dao();
        if (dao.serch(book.getTitle()) != null){
            result=dao.modifyData(book);
        }else{
            result=dao.setData(book);
        }
        return result;
    }
    
    public int delete (String title){
        int result=0;
        dao= new Dao();
        if (dao.serch(title) != null){
            result=dao.deleteData(title);
        }
        return result;
    }
    
    public Book bookSerch (String title){
       return new Dao().serch(title);
    }
    
    public  ArrayList<Book> bookSerch (){
        return new Dao().serchAll();
    }
}
